#!/bin/bash
npm install
rm log.log
touch log.log
server_js=`pgrep node`
if [ -z "$server_js" ]; then
  echo "No process"
else
  kill -9 $server_js
fi
node server.js >> ~/log.log 2>&1 &
